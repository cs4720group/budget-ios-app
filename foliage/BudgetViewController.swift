//
//  BudgetViewController.swift
//  foliage
//
//  Created by Levi Payne on 10/31/15.
//  Copyright © 2015 Xinyue Yao. All rights reserved.
//

import UIKit

class BudgetViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var otherTotal: UILabel!
    @IBOutlet weak var groceriesTotal: UILabel!
    @IBOutlet weak var restaurantsTotal: UILabel!
    @IBOutlet weak var shoppingTotal: UILabel!
    @IBOutlet weak var travelTotal: UILabel!
    @IBOutlet weak var businessTotal: UILabel!
    @IBOutlet weak var educationTotal: UILabel!
    
    @IBOutlet weak var otherLimit: UITextField!
    @IBOutlet weak var groceriesLimit: UITextField!
    @IBOutlet weak var restaurantsLimit: UITextField!
    @IBOutlet weak var shoppingLimit: UITextField!
    @IBOutlet weak var travelLimit: UITextField!
    @IBOutlet weak var businessLimit: UITextField!
    @IBOutlet weak var educationLimit: UITextField!
    
    var otherLimitVal = 0;
    var groceriesLimitVal = 0;
    var restaurantsLimitVal = 0;
    var shoppingLimitVal = 0;
    var travelLimitVal = 0;
    var businessLimitVal = 0;
    var educationLimitVal = 0;
    
    var limits: NSMutableArray = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    var totals: NSMutableArray = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    
    //dismiss number pad
    
    func didTapView(){
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //dismiss number pad
        
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.addTarget(self, action: "didTapView")
        self.view.addGestureRecognizer(tapRecognizer)
        
        
        
        scrollView.contentSize.height = 600
        
        bvc = self
        
        otherLimit.delegate = self
        groceriesLimit.delegate = self
        restaurantsLimit.delegate = self
        shoppingLimit.delegate = self
        travelLimit.delegate = self
        businessLimit.delegate = self
        educationLimit.delegate = self
        
        load()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changedLimit(sender: UITextField) {
        var mutableLimit = 0.0
        var mutableTotal = 0.0
        var mutableLabel = UILabel()
        var index = 99 // Invalid index to be checked at the end
        
        
        switch (sender) {
        case otherLimit:
            let otherLimitVal = Double((otherLimit.text)!)
            let otherTotalVal = Double((otherTotal.text)!)
            if (otherLimitVal == nil) {
                mutableLimit = 0.0;
                otherLimit.text = ""
            }
            else {
                mutableLimit = otherLimitVal!
            }
            mutableTotal = otherTotalVal!
            mutableLabel = otherTotal
            index = 0
            break
        case groceriesLimit:
            let groceriesLimitVal = Double((groceriesLimit.text)!)
            let groceriesTotalVal = Double((groceriesTotal.text)!)
            if (groceriesLimitVal == nil) {
                mutableLimit = 0.0;
                groceriesLimit.text = ""
            }
            else {
                mutableLimit = groceriesLimitVal!
            }
            mutableTotal = groceriesTotalVal!
            mutableLabel = groceriesTotal
            index = 1
            break
        case restaurantsLimit:
            let restaurantsLimitVal = Double((restaurantsLimit.text)!)
            let restaurantsTotalVal = Double((restaurantsTotal.text)!)
            if (restaurantsLimitVal == nil) {
                mutableLimit = 0.0;
                restaurantsLimit.text = ""
            }
            else {
                mutableLimit = restaurantsLimitVal!
            }
            mutableTotal = restaurantsTotalVal!
            mutableLabel = restaurantsTotal
            index = 2
            break
        case shoppingLimit:
            let shoppingLimitVal = Double((shoppingLimit.text)!)
            let shoppingTotalVal = Double((shoppingTotal.text)!)
            if (shoppingLimitVal == nil) {
                mutableLimit = 0.0;
                shoppingLimit.text = ""
            }
            else {
                mutableLimit = shoppingLimitVal!
            }
            mutableTotal = shoppingTotalVal!
            mutableLabel = shoppingTotal
            index = 3
            break
        case travelLimit:
            let travelLimitVal = Double((travelLimit.text)!)
            let travelTotalVal = Double((travelTotal.text)!)
            if (travelLimitVal == nil) {
                mutableLimit = 0.0;
                travelLimit.text = ""
            }
            else {
                mutableLimit = travelLimitVal!
            }
            mutableTotal = travelTotalVal!
            mutableLabel = travelTotal
            index = 4
            break
        case businessLimit:
            let businessLimitVal = Double((businessLimit.text)!)
            let businessTotalVal = Double((businessTotal.text)!)
            if (businessLimitVal == nil) {
                mutableLimit = 0.0;
                businessLimit.text = ""
            }
            else {
                mutableLimit = businessLimitVal!
            }
            mutableTotal = businessTotalVal!
            mutableLabel = businessTotal
            index = 5
            break
        case educationLimit:
            let educationLimitVal = Double((educationLimit.text)!)
            let educationTotalVal = Double((educationTotal.text)!)
            if (educationLimitVal == nil) {
                mutableLimit = 0.0;
                educationLimit.text = ""
            }
            else {
                mutableLimit = educationLimitVal!
            }
            mutableTotal = educationTotalVal!
            mutableLabel = educationTotal
            index = 6
            break
        default:
            break
        }
        
        if (mutableTotal > mutableLimit) {
            mutableLabel.textColor = UIColor.redColor()
        }
        else {
            mutableLabel.textColor = UIColor.greenColor()
        }
        
        if (index != 99) {
            limits.replaceObjectAtIndex(index, withObject: mutableLimit)
        }
        
        save()
    }
    

    
    func transactionWasAdded(category: NSString, value: Double) {
        var mutableLabel = UILabel()
        var mutableLimit = ""
        var index = 99 // Invalid index to be checked at the end
        
        switch (category) {
        case "Other":
            mutableLabel = otherTotal
            mutableLimit = otherLimit.text!
            index = 0
            break
        case "Groceries":
            mutableLabel = groceriesTotal
            mutableLimit = groceriesLimit.text!
            index = 1
            break
        case "Restaurants":
            mutableLabel = restaurantsTotal
            mutableLimit = restaurantsLimit.text!
            index = 2
            break
        case "Shopping":
            mutableLabel = shoppingTotal
            mutableLimit = shoppingLimit.text!
            index = 3
            break
        case "Travel":
            mutableLabel = travelTotal
            mutableLimit = travelLimit.text!
            index = 4
            break
        case "Business":
            mutableLabel = businessTotal
            mutableLimit = businessLimit.text!
            index = 5
            break
        case "Education":
            mutableLabel = educationTotal
            mutableLimit = educationLimit.text!
            index = 6
            break
        default:
            break
        }
        
        var sum = Double((mutableLabel.text)!)
        let limit = Double(mutableLimit)
        if (sum != nil) {
            sum = sum! + value
            mutableLabel.text = String(format: "%.2f", sum!)
            if (sum > limit) {
                mutableLabel.textColor = UIColor.redColor()
            }
        }
        
        if (index != 99) {
            totals.replaceObjectAtIndex(index, withObject: sum!)
        }
        
        save()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true;
        
    }
    
    func save() {
        let limitsPath = NSTemporaryDirectory() + "Limits.txt"
        let totalsPath = NSTemporaryDirectory() + "Totals.txt"
        
        if limits.writeToFile(limitsPath, atomically: true) {
            let readArray:NSArray? = NSArray(contentsOfFile: limitsPath)
            if let _ = readArray {
                print("Limits successfully saved")
            }
            else {
                print("Something went wrong when saving limits")
            }
        }
        
        if totals.writeToFile(totalsPath, atomically: true) {
            let readArray:NSArray? = NSArray(contentsOfFile: totalsPath)
            if let _ = readArray {
                print("Totals successfully saved")
            }
            else {
                print("Something went wrong when saving totals")
            }
        }
    }
    
    func load() {
        // Load Limits
        let limitsPath = NSTemporaryDirectory() + "Limits.txt"
        let limitsArray:NSMutableArray? = NSMutableArray(contentsOfFile: limitsPath)
        if let array = limitsArray {
            print("Limits successfully loaded")
            
            self.limits = array
            
            // Insert Limits
            otherLimit.text = String(format:"%.2f", limits[0].doubleValue)
            groceriesLimit.text = String(format:"%.2f", limits[1].doubleValue)
            restaurantsLimit.text = String(format:"%.2f", limits[2].doubleValue)
            shoppingLimit.text = String(format:"%.2f", limits[3].doubleValue)
            travelLimit.text = String(format:"%.2f", limits[4].doubleValue)
            businessLimit.text = String(format:"%.2f", limits[5].doubleValue)
            educationLimit.text = String(format:"%.2f", limits[6].doubleValue)
        }
        else {
            print("Something went wrong when loading limits")
        }
        
        // Load Totals
        let totalsPath = NSTemporaryDirectory() + "Totals.txt"
        let totalsArray:NSMutableArray? = NSMutableArray(contentsOfFile: totalsPath)
        if let array = totalsArray {
            print("Totals successfully saved")
            
            self.totals = array
            
            otherTotal.text = String(format:"%.2f", totals[0].doubleValue)
            if (limits[0].doubleValue < totals[0].doubleValue) {
                otherTotal.textColor = UIColor.redColor()
            }
            groceriesTotal.text = String(format:"%.2f", totals[1].doubleValue)
            if (limits[1].doubleValue < totals[1].doubleValue) {
                groceriesTotal.textColor = UIColor.redColor()
            }
            restaurantsTotal.text = String(format:"%.2f", totals[2].doubleValue)
            if (limits[2].doubleValue < totals[2].doubleValue) {
                restaurantsTotal.textColor = UIColor.redColor()
            }
            shoppingTotal.text = String(format:"%.2f", totals[3].doubleValue)
            if (limits[3].doubleValue < totals[3].doubleValue) {
                shoppingTotal.textColor = UIColor.redColor()
            }
            travelTotal.text = String(format:"%.2f", totals[4].doubleValue)
            if (limits[4].doubleValue < totals[4].doubleValue) {
                travelTotal.textColor = UIColor.redColor()
            }
            businessTotal.text = String(format:"%.2f", totals[5].doubleValue)
            if (limits[5].doubleValue < totals[5].doubleValue) {
                businessTotal.textColor = UIColor.redColor()
            }
            educationTotal.text = String(format:"%.2f", totals[6].doubleValue)
            if (limits[6].doubleValue < totals[6].doubleValue) {
                educationTotal.textColor = UIColor.redColor()
            }
        }
        else {
            print("Something went wrong when saving totals")
        }
    }
    
    @IBAction func reset(sender: AnyObject) {
        limits = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        totals = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        
        otherLimit.text = ""
        groceriesLimit.text = ""
        restaurantsLimit.text = ""
        shoppingLimit.text = ""
        travelLimit.text = ""
        businessLimit.text = ""
        educationLimit.text = ""
        
        otherTotal.text = "0"
        groceriesTotal.text = "0"
        restaurantsTotal.text = "0"
        shoppingTotal.text = "0"
        travelTotal.text = "0"
        businessTotal.text = "0"
        educationTotal.text = "0"
        
        otherTotal.textColor = UIColor.greenColor()
        groceriesTotal.textColor = UIColor.greenColor()
        restaurantsTotal.textColor = UIColor.greenColor()
        shoppingTotal.textColor = UIColor.greenColor()
        travelTotal.textColor = UIColor.greenColor()
        businessTotal.textColor = UIColor.greenColor()
        educationTotal.textColor = UIColor.greenColor()
        
        save()
    }
    

}
