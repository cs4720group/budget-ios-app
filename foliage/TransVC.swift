//
//  TransVC.swift
//  foliage
//
//  Created by Xinyue Yao on 10/27/15.
//  Copyright © 2015 Xinyue Yao. All rights reserved.
//

import UIKit
import CoreData

var transList = [String]()
var loaded = false

class TransVC: UIViewController, UIScrollViewDelegate {
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    @IBOutlet weak var table: UITableView!
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return transList.count
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        cell.textLabel?.text = transList[indexPath.row]
        return cell
    }
    
//    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
//        if editingStyle == .Delete {
//            transList.removeAtIndex(indexPath.row)
//            table.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
//        }
//    }
//    
    override func viewDidAppear(animated: Bool) {
        table.reloadData()
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        loadTransactions()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resourc
    }
    
    func saveTrans(){
    
    }
    
    func loadTransactions() {
        loaded = true
        let fetchRequest = NSFetchRequest(entityName: "Transaction")
        let dateSort = NSSortDescriptor(key: "date", ascending: false)
        
        fetchRequest.sortDescriptors = [dateSort]
        
        do {
            let transactions = try managedObjectContext.executeFetchRequest(fetchRequest) as! [Transaction]
            for transaction in transactions {
                var value = transaction.name
                value += "        "
                value += String(format: "%.2f", transaction.cost)
                value += "        "
                value += transaction.category
                
                transList.append(value)
            }
        } catch let error as NSError {
            print(error)
        }
    }

}
