//
//  ViewController.swift
//  foliage
//
//  Created by Xinyue Yao on 10/16/15.
//  Copyright © 2015 Xinyue Yao. All rights reserved.
//

import UIKit
import MapKit
import AVFoundation
import CoreData

var bvc: BudgetViewController!

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // MARK: Properties

    @IBOutlet weak var scrollView: UIScrollView!
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var picker: UIPickerView!
    
    var audioPlayer = AVAudioPlayer()
    
    var emptyString = ""
    var someList = [String]()
    var pickerData = [String]()
    var category = "Other"
    
    var controller:UIAlertController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        textField.delegate = self
        picker.delegate = self
        priceField.delegate = self
        
        picker.dataSource = self
        textField.placeholder = "Purchase"
        priceField.placeholder = "Price"
        pickerData = ["Other", "Groceries", "Restaurants", "Shopping", "Travel", "Business", "Education"]
        
        picker.selectRow(3, inComponent: 0, animated: false)

        
        scrollView.contentSize.height = 500
        
        
        //dismiss number pad
        
        let tapRecognizer = UITapGestureRecognizer()
        tapRecognizer.addTarget(self, action: "didTapView")
        self.view.addGestureRecognizer(tapRecognizer)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        category = pickerData[row]
        return pickerData[row]
    }
    
    @IBAction func addTransaction(sender: AnyObject) {
        category = pickerView(picker, titleForRow: picker.selectedRowInComponent(0), forComponent: 0)!
        let name = textField.text!
        let cost = Double(priceField.text!)
        let date = NSDate()
        
        if (name != "" && cost != nil) {
            createNewTransaction(name, withDate: date, withCategory: category, withCost: cost!)
            
            var value = name
            value += "        "
            value += priceField.text!
            value += "        "
            value += category
            
            if (!transList.isEmpty || loaded) {
                transList.insert(value, atIndex: 0)
            }
            
            let price = Double((priceField.text)!)
            if (price != nil) {
                bvc.transactionWasAdded(category, value: price!)
            }
            
            category = "Other"
            priceField.text = ""
            textField.text = ""
            
            //audio
            
            let fileLoc = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("chaching", ofType: "mp3")!)
            
            do {
                audioPlayer = try AVAudioPlayer(contentsOfURL: fileLoc)
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                try AVAudioSession.sharedInstance().setActive(true)
            }
            catch{
                print("Bad audio, didn't work.")
            }
            
            audioPlayer.play()
        }
        else {
            showAlert("One or more fields were unpopulated or were given invalid input. Please try again.")
        }
        
    }
    
    func didTapView(){
        self.view.endEditing(true)
    }
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("YOHOHOHOHO")
        priceField.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true;
        
    }
    
    //dismiss number pad
    

    

 
    @IBAction func doneEditing(sender: AnyObject) {
        priceField.resignFirstResponder()
        textField.resignFirstResponder()
    }
    
    func createNewTransaction(name: String, withDate date: NSDate, withCategory category: String, withCost cost: Double) {
       let transaction = NSEntityDescription.insertNewObjectForEntityForName("Transaction", inManagedObjectContext: managedObjectContext) as! foliage.Transaction
        transaction.category = category
        transaction.name = name
        transaction.cost = cost
        transaction.date = date.timeIntervalSince1970
        
        do {
            try managedObjectContext.save()
            print("\(name) transaction saved!")
        } catch let error as NSError {
            print("Failed to save transaction. Error: \(error)")
        }
    }
    
    func showAlert(text: NSString) {
        controller = UIAlertController(title: "Invalid Input",
            message: (text as String),
            preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "Done",
            style: UIAlertActionStyle.Default,
            handler: {(paramAction:UIAlertAction!) in
                print((text as String), terminator: "")
        })
        
        controller!.addAction(action)
        self.presentViewController(controller!, animated: true, completion: nil)
    }
    
}

//stackoverflow.com/questions/27878732


