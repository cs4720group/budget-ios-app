//
//  GpsViewController.swift
//  foliage
//
//  Created by Xinyue Yao on 11/1/15.
//  Copyright © 2015 Xinyue Yao. All rights reserved.
//



import UIKit
import MapKit

class GpsViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var lon: UILabel!

    @IBOutlet weak var lat: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    var curLon = ""
    var curLat = ""
    
    var bankLocationList = [(name: String, lat: Double, lng: Double)]()
    var annotationList = [MKPointAnnotation]()
    
    var first_start = true
    
    
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print("loaded")

        self.locationManager.delegate = self
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        self.locationManager.requestWhenInUseAuthorization()
        
        
   //        var jDict = json.dictionary
        
    }
    
    func parseJSON(json: JSON){
        
        for result in json["results"].arrayValue{
//            print(result["name"].stringValue)
            bankLocationList.append((result["name"].stringValue, result["geometry"]["location"]["lat"].doubleValue, result["geometry"]["location"]["lng"].doubleValue))
            
        }
    
    }

    
    override func viewDidAppear(animated: Bool) {
        
        self.locationManager.startUpdatingLocation()
        
        self.mapView.showsUserLocation = true
        
        
        


        print("appeared")
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let location = locations.last
        
        let latitudeText:String = "\(location!.coordinate.latitude)"
        let longitudeText:String = "\(location!.coordinate.longitude)"
        
        
        lon.text = latitudeText
        lat.text = longitudeText
        
//        print(location!.coordinate.latitude)
        
        if first_start{
            let user_location = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
            
            let region = MKCoordinateRegion(center: user_location, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.03))
            
            self.mapView.setRegion(region, animated: true)
            
            first_start = false
            
            curLat = String(format: "%f",(locationManager.location?.coordinate.latitude)!)
            curLon = String(format: "%f",(locationManager.location?.coordinate.longitude)!)
            
            let full_address = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+curLat+","+curLon+"&radius=5000&types=bank&key=AIzaSyCyGtJCfhxAyC5oFIj_Lxboiua2sD04rVA"
            
            print(full_address)
            
            //swifty json
            
            let url = NSURL(string: full_address)
            let data = NSData(contentsOfURL: url!)
            let json = JSON(data: data!)
            
            parseJSON(json)
            
            print(bankLocationList)
            
            //adding bank locations to map
            
            for tup in bankLocationList{
                let tuplat:CLLocationDegrees = tup.lat
                let tuplon:CLLocationDegrees = tup.lng
                
                let bankCoord = CLLocationCoordinate2D(latitude: tuplat, longitude: tuplon)
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = bankCoord
                annotation.title = tup.name
                
                annotationList.append(annotation)
                
                mapView.addAnnotation(annotation)
            }
        
        }
        

    }
    
    override func viewDidDisappear(animated: Bool) {
        
        print("view disappeared")
        self.locationManager.stopUpdatingLocation()
        for flag in annotationList{
            mapView.removeAnnotation(flag)
        }
        
        annotationList = []
        bankLocationList = []
        first_start = true
    }
}

//veasoftware.com/tutorials/2015/7/25/map-view-current-location-in-swift-xcode-7-ios-9-tutorial
