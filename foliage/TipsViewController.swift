//
//  TipsViewController.swift
//  foliage
//
//  Created by Xinyue Yao on 11/11/15.
//  Copyright © 2015 Xinyue Yao. All rights reserved.
//

import UIKit
import CoreMotion

class TipsViewController: UIViewController {
    
    lazy var motionManager = CMMotionManager()
    var tips_list: [String] = ["Make your own gifts instead of buying stuff from the store.", "Invite friends over instead of going out.", "Negotiate rates with your credit card company or complete a balance transfer.", "Avoid convenience foods and fast food.", "Turn off the lights.", "Buy quality appliances that will last.", "Clean or change out your car’s air filter.", "Quit using credit cards.", "Plan your meals around your grocery store’s flyer.", "Cancel unused club memberships.", "Do holiday shopping right after the holidays.", "Prepare some meals at home.", "Swap babysitting with neighbors.", "Brown bag your lunch.", "Try to fix things yourself.", "Take public transportation.", "Buy staples in bulk.", "Pack food for road trips."]
    
    
    var count = 17
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func motionEnded(motion: UIEventSubtype,
        withEvent event: UIEvent?) {
            
            let ind = Int(arc4random_uniform(17))
            let tip = tips_list[ind]
            
            if motion == .MotionShake{
                let controller = UIAlertController(title: "Tip",
                    message: tip,
                    preferredStyle: .Alert)
                
                controller.addAction(UIAlertAction(title: "OK",
                    style: .Default,
                    handler: nil))
                
                presentViewController(controller, animated: true, completion: nil)
                
            }
            
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
