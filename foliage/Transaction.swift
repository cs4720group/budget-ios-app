//
//  Transaction.swift
//  foliage
//
//  Created by Levi Payne on 11/1/15.
//  Copyright © 2015 Xinyue Yao. All rights reserved.
//

import Foundation
import CoreData

@objc(Transaction) class Transaction: NSManagedObject {
    
    @NSManaged var category: String
    @NSManaged var name: String
    @NSManaged var cost: Double
    @NSManaged var date: Double
    
}
